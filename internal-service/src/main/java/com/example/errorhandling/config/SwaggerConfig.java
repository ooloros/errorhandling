package com.example.errorhandling.config;

import com.example.errorhandling.person.dto.ErrorsDto;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api(TypeResolver typeResolver) {
        return new Docket(DocumentationType.SWAGGER_2)
                .additionalModels(typeResolver.resolve(ErrorsDto.class))
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/person"))
                .build();
    }
}
