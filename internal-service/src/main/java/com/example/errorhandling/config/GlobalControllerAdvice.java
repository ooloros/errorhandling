package com.example.errorhandling.config;

import com.example.errorhandling.person.dto.ErrorsDto;
import com.example.errorhandling.person.dto.ErrorsDto.ErrorDTO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ControllerAdvice
@Slf4j
public class GlobalControllerAdvice {

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleBadRequest(MethodArgumentNotValidException ex) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.parseMediaType(ErrorsDto.MEDIA_TYPE));
        return new ResponseEntity<>(ErrorsDto.from(ex), httpHeaders, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(BusinessException ex) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.parseMediaType(ErrorsDto.MEDIA_TYPE));
        return new ResponseEntity<>(ErrorsDto.from(ex), httpHeaders, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public void handleInternalServerError(RuntimeException e) {
        log.error(e.getMessage(), e);
    }

    @Getter
    public static class BusinessException extends RuntimeException {
        private final transient List<ErrorDTO> errors;

        public BusinessException(List<ErrorDTO> errors) {
            super();
            this.errors = errors;
        }
    }
}
