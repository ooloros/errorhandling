package com.example.errorhandling.person.dto;

import com.example.errorhandling.config.GlobalControllerAdvice.BusinessException;
import io.swagger.annotations.ApiModel;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

@ApiModel
public record ErrorsDto(List<ErrorDTO> errors) {
    public static final String MEDIA_TYPE = "application/vnd.star.errors+json;version=1.0";
    public static final String EXAMPLE_CONTENT = """
            {
              "errors": [
                {
                  "code": "NotNull",
                  "defaultMessage": "must not be null",
                  "field": "name"
                }
              ]
            }
            """;

    public static ErrorsDto from(MethodArgumentNotValidException ex) {
        return new ErrorsDto(ex.getFieldErrors().stream().map(ErrorDTO::new).toList());
    }

    public static ErrorsDto from(BusinessException ex) {
        return new ErrorsDto(ex.getErrors());
    }

    public record ErrorDTO(String code, String defaultMessage, String field) {

        public ErrorDTO(FieldError fieldError) {
            this(fieldError.getCode(), fieldError.getDefaultMessage(), fieldError.getField());
        }
    }
}

