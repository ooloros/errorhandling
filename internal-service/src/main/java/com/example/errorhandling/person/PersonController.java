package com.example.errorhandling.person;

import com.example.errorhandling.person.dto.CreatePersonRequestDto;
import com.example.errorhandling.person.dto.PersonResponseDto;
import com.example.errorhandling.person.dto.ErrorsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/person")
public class PersonController {
    private final PersonService personService;

    @PostMapping(
            consumes = CreatePersonRequestDto.MEDIA_TYPE,
            produces = PersonResponseDto.MEDIA_TYPE)
    @Operation(
            summary = "create a new person",
            responses = @ApiResponse(description = "Validation errors", responseCode = "400",
                    content = @Content(
                            mediaType = ErrorsDto.MEDIA_TYPE,
                            examples = @ExampleObject(ErrorsDto.EXAMPLE_CONTENT),
                            schema = @Schema(implementation = ErrorsDto.class)
                    ))
    )
    public PersonResponseDto postData(@Valid @RequestBody CreatePersonRequestDto input) {
        return personService.save(input);
    }
}
