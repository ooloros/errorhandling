package com.example.errorhandling.person.dto;

import java.util.UUID;

public record PersonResponseDto(UUID id, String name) {
    public static final String MEDIA_TYPE = "application/vnd.star.person+json;version=1.0";
}
