package com.example.errorhandling.person.entity;

import java.util.UUID;

public record Person(UUID id, String name) {
}
