package com.example.errorhandling.person;

import com.example.errorhandling.person.entity.Person;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public class PersonRepository {
    public Person save(Person person) {
        return new Person(UUID.randomUUID(), person.name());
    }
}
