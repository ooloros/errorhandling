package com.example.errorhandling.person.dto;

import javax.validation.constraints.NotNull;

public record CreatePersonRequestDto(@NotNull(message = "this must not be null") String name) {
    public static final String MEDIA_TYPE = "application/vnd.star.person.create-request+json;version=1.0";
}
