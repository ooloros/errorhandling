package com.example.errorhandling.person;

import com.example.errorhandling.person.dto.CreatePersonRequestDto;
import com.example.errorhandling.person.dto.PersonResponseDto;
import com.example.errorhandling.person.entity.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;

    public PersonResponseDto save(CreatePersonRequestDto request) {
        Person person = new Person(null, request.name());
        Person savedPerson = personRepository.save(person);

        return new PersonResponseDto(savedPerson.id(), savedPerson.name());
    }
}
