package com.example.errorhandling.person;

import com.example.errorhandling.config.GlobalControllerAdvice.BusinessException;
import com.example.errorhandling.person.dto.CreatePersonRequestDto;
import com.example.errorhandling.person.dto.ErrorsDto;
import com.example.errorhandling.person.dto.ErrorsDto.ErrorDTO;
import com.example.errorhandling.person.dto.PersonResponseDto;
import com.example.errorhandling.person.entity.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PersonController.class)
class PersonControllerTest {
    private static final Person NEW_PERSON = new Person(UUID.randomUUID(), "name-" + UUID.randomUUID());
    private static final String EXAMPLE_CREATE_PERSON_REQUEST_INPUT = """
            {"name": "%s"}
            """.formatted(NEW_PERSON.name());
    @MockBean
    private PersonService personService;
    @Autowired
    private MockMvc mvc;

    @Nested
    class Success {
        @BeforeEach
        void setup() {
            doReturn(new PersonResponseDto(
                    NEW_PERSON.id(),
                    NEW_PERSON.name())
            ).when(personService).save(any(CreatePersonRequestDto.class));
        }

        @Test
        void shouldHave200Response() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(status().isOk());
        }

        @Test
        void shouldHaveResponseContentType() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(header().string("content-type", PersonResponseDto.MEDIA_TYPE));
        }

        @Test
        void shouldHaveResponseContent() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(content().json("""
                            {
                              "id": "%s",
                              "name": "%s"
                            }
                            """.formatted(NEW_PERSON.id(), NEW_PERSON.name()), true));
        }
    }

    @Nested
    class InternalServer {
        @BeforeEach
        void setup() {
            doThrow(new NullPointerException())
                    .when(personService).save(any(CreatePersonRequestDto.class));
        }

        @Test
        void shouldHave500Response() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(status().isInternalServerError());
        }
    }

    @Nested
    class BadRequest {

        @Test
        void shouldHave400Response() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content("{}"))
                    .andExpect(status().isBadRequest());
        }

        @Test
        void shouldHaveBusinessErrorResponse() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content("{}"))
                    .andExpect(status().isBadRequest());
        }

        @Test
        void shouldHaveResponseContent() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content("{}"))
                    .andExpect(content().json("""
                            {
                              "errors": [{
                                "code": "NotNull",
                                "field": "name"
                              }]
                            }
                            """, false));
        }

        @Test
        void shouldHaveResponseContentType() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content("{}"))
                    .andExpect(header().string("content-type", ErrorsDto.MEDIA_TYPE));
        }
    }

    @Nested
    class BusinessError {
        @BeforeEach
        void setup() {
            doThrow(new BusinessException(singletonList(new ErrorDTO("SomeBusinessProblem", null, null))))
                    .when(personService).save(any(CreatePersonRequestDto.class));
        }

        @Test
        void shouldHave400Response() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(status().isBadRequest());
        }

        @Test
        void shouldHaveResponseContentType() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(header().string("content-type", ErrorsDto.MEDIA_TYPE));
        }

        @Test
        void shouldHaveResponseContent() throws Exception {
            mvc.perform(post("/person")
                            .contentType(CreatePersonRequestDto.MEDIA_TYPE)
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(content().json("""
                            {
                              "errors": [{
                                "code": "SomeBusinessProblem"
                              }]
                            }
                            """, false));
        }
    }
}
