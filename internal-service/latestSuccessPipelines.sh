#!/usr/bin/env bash
set -e

API_TOKEN=$1
CI_API_V4_URL="https://gitlab.com/api/v4"

services="$(awk '/pipelineId/','/}/' variables.auto.tfvars | egrep "\s*(=)\s*\"" | awk '{print($1)}')"
#services="auth-service"

for serv in $services
do
  repo=$serv
  echo "$serv"
  if [[ $serv =~ "star2-frontend" ]]; then
    repo="star2-frontend"
  fi
  projects=$(curl -sH "JOB-TOKEN: $API_TOKEN" $CI_API_V4_URL/projects?simple=true\&search=$repo)
  echo "$projects"
  projectId=$(echo "$projects" | jq -rc ".[] | select( .path_with_namespace == \"ooloros/"$repo"\") | .id")
  echo "$pipelineId"
  pipelines=$(curl -sH "JOB-TOKEN: $API_TOKEN" $CI_API_V4_URL/projects/$projectId/pipelines?status=success\&ref=ddd)

  echo "$pipelines"
  if [[ $serv =~ "star2-frontend" ]]; then
    pipelineIds=$(echo "$pipelines" | jq -rc '.[] | .id')
    for id in $pipelineIds
    do
      bridges=$(curl -sH "JOB-TOKEN: $API_TOKEN" $CI_API_V4_URL/projects/$projectId/pipelines/$id/bridges)
      downStreamIds=$(echo "$bridges" | jq '.[] | .downstream_pipeline.id')
      for dsId in $downStreamIds
      do
        variables=$(curl -sH "JOB-TOKEN: $API_TOKEN" $CI_API_V4_URL/projects/$projectId/pipelines/$dsId/variables)
        app=$(echo "$variables" | jq -rc '.[] | select( .key == "APP" ) | .value')
        if [[ "$repo-$app" = "$serv" ]]; then
          pipelineId="$dsId"
          break
        fi
      done
      break
    done
  else
    pipelineId=$(echo "$pipelines" | jq '.[0] | .id')
  fi

  echo "    $serv = \"$pipelineId\","
done
