package com.example.apigw.generic;

import com.example.apigw.config.CommonTestConfiguration.ServiceHandler;
import com.example.apigw.generic.GenericService.RpcResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Map;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ComponentScan("com.example.apigw.config")
@ActiveProfiles({"test", "http-servermock-test"})
class GenericControllerTest {
    public static final String RPC_PERSON_ENDPOINT = "/rpc/person";
    private static final Map<String, Object> NEW_PERSON = Map.of("id", UUID.randomUUID(), "name", "name-" + UUID.randomUUID());
    private static final String EXAMPLE_CREATE_PERSON_REQUEST_INPUT = """
            {"name": "%s"}
            """.formatted(NEW_PERSON.get("id"));
    @MockBean
    private ServiceHandler serviceHandler;

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mvc;

    @BeforeEach
    void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Nested
    class Success {
        @BeforeEach
        void setup() {
            doReturn(new RpcResponse(
                    "application/vnd.star.person+json;version=1.0",
                    NEW_PERSON)
            ).when(serviceHandler).execute(any());
        }

        @Test
        void shouldHave200Response() throws Exception {
            mvc.perform(post(RPC_PERSON_ENDPOINT)
                            .contentType("application/vnd.star.person.create-request+json;version=1.0")
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(status().isOk());
        }

        @Test
        void shouldHaveResponseContentType() throws Exception {
            mvc.perform(post(RPC_PERSON_ENDPOINT)
                            .contentType("application/vnd.star.person.create-request+json;version=1.0")
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(header().string("content-type", "application/vnd.star.person+json;version=1.0"));
        }

        @Test
        @Tag("smoke")
        void shouldHaveResponseContent() throws Exception {
            mvc.perform(post(RPC_PERSON_ENDPOINT)
                            .contentType("application/vnd.star.person.create-request+json;version=1.0")
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(content().json("""
                            {
                              "id": "%s",
                              "name": "%s"
                            }
                            """.formatted(NEW_PERSON.get("id"), NEW_PERSON.get("name")), true));
        }
    }

    @Nested
    class InternalServer {
        @BeforeEach
        void setup() {
            doThrow(new RuntimeException())
                    .when(serviceHandler).execute(any());
        }

        @Test
        void shouldHave500Response() throws Exception {
            mvc.perform(post(RPC_PERSON_ENDPOINT)
                            .contentType("application/vnd.star.person.create-request+json;version=1.0")
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(status().isInternalServerError());
        }
    }

    @Nested
    class BusinessError {
        @BeforeEach
        void setup() {
            doThrow(new BusinessException(
                    "application/vnd.star.errors+json;version=1.0",
                    singletonList(Map.of("code", "SomeBusinessProblem")))
            ).when(serviceHandler).execute(any());
        }

        @Test
        void shouldHave400Response() throws Exception {
            mvc.perform(post(RPC_PERSON_ENDPOINT)
                            .contentType("application/vnd.star.person.create-request+json;version=1.0")
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(status().isBadRequest());
        }

        @Test
        void shouldHaveResponseContentType() throws Exception {
            mvc.perform(post(RPC_PERSON_ENDPOINT)
                            .contentType("application/vnd.star.person.create-request+json;version=1.0")
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(header().string("content-type", "application/vnd.star.errors+json;version=1.0"));
        }

        @Test
        @Tag("smoke")
        void shouldHaveResponseContent() throws Exception {
            mvc.perform(post(RPC_PERSON_ENDPOINT)
                            .contentType("application/vnd.star.person.create-request+json;version=1.0")
                            .content(EXAMPLE_CREATE_PERSON_REQUEST_INPUT))
                    .andExpect(content().json("""
                            {
                              "errors": [{
                                "code": "SomeBusinessProblem"
                              }]
                            }
                            """, false));
        }
    }
}
