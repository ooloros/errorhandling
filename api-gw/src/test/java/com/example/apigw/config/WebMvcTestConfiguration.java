package com.example.apigw.config;

import com.example.apigw.config.CommonTestConfiguration.ServiceHandler;
import com.example.apigw.generic.GenericService;
import com.example.apigw.generic.clientexecutor.RpcExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

@Configuration
@Profile("webmvc-test")
public class WebMvcTestConfiguration {
    @Autowired(required = false)
    private ServiceHandler serviceHandler;

    @Bean
    @Primary
    public GenericService genericService() {
        var service = mock(GenericService.class);
        doAnswer(a -> serviceHandler.execute(a.getArgument(0))).when(service).execute(any());
        return service;
    }

    @Bean
    @Primary
    public RpcExecutor rpcExecutor() {
        return mock(RpcExecutor.class);
    }
}
