package com.example.apigw.config;

import com.example.apigw.generic.GenericService.RpcRequest;
import com.example.apigw.generic.GenericService.RpcResponse;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfilesResolver;

@Configuration
public class CommonTestConfiguration {

    public interface ServiceHandler {
        RpcResponse execute(RpcRequest input);
    }

    public static class DynamicActiveProfileResolver implements ActiveProfilesResolver {
        @Override
        public String[] resolve(final Class<?> aClass) {
            final String activeProfile = System.getProperty("spring.profiles.active");
            return new String[]{activeProfile == null ? "webmcv-test" : activeProfile};
        }
    }
}
