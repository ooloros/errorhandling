package com.example.apigw.config;

import com.example.apigw.config.CommonTestConfiguration.ServiceHandler;
import com.example.apigw.generic.BusinessException;
import com.example.apigw.generic.GenericService.RpcRequest;
import com.example.apigw.generic.GenericService.RpcResponse;
import com.example.apigw.generic.clientexecutor.http.HttpClientConfigurationProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;

@Slf4j
@Configuration
@Profile("http-servermock-test")
public class HttpWiremockTestConfiguration {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired(required = false)
    private ServiceHandler serviceHandler;
    private HttpServer httpServer;

    @Autowired
    @SuppressWarnings("unchecked")
    public void setupHttpServer() throws IOException {
        httpServer = HttpServer.create(new InetSocketAddress(9999), 0);
        httpServer.createContext("/person", (exchange) -> {
            try {
                RpcRequest req = new RpcRequest(
                        exchange.getRequestMethod(),
                        exchange.getRequestURI().toString(),
                        exchange.getRequestHeaders().getFirst("accept"),
                        exchange.getRequestHeaders().getFirst("content-type"),
                        objectMapper.readValue(exchange.getRequestBody(), Map.class)
                );
                RpcResponse response = serviceHandler.execute(req);
                writeToOutput(HttpExchangeResponseWrapper.builder()
                        .exchange(exchange)
                        .status(HttpStatus.OK)
                        .contentType(response.mediaType())
                        .content(response.responseBody())
                        .build());
            } catch (BusinessException e) {
                writeToOutput(HttpExchangeResponseWrapper.builder()
                        .exchange(exchange)
                        .status(HttpStatus.BAD_REQUEST)
                        .contentType(e.getMediaType())
                        .content(Map.of("errors", e.getErrors()))
                        .build());
            } catch (Exception e) {
                exchange.sendResponseHeaders(HttpStatus.INTERNAL_SERVER_ERROR.value(), 0);
                exchange.close();
            }
        });
        httpServer.setExecutor(null);
        httpServer.start();
    }

    @Bean
    @Primary
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:9999"));
        return restTemplate;
    }

    private void writeToOutput(HttpExchangeResponseWrapper resp) throws IOException {
        resp.exchange.getResponseHeaders().set(HttpHeaders.CONTENT_TYPE, resp.contentType);
        byte[] responseBytes = objectMapper.writeValueAsBytes(resp.content);
        resp.exchange.sendResponseHeaders(resp.status.value(), responseBytes.length);
        resp.exchange.getResponseBody().write(responseBytes);
        resp.exchange.close();
    }

    @PreDestroy
    public void stopHttpserver() {
        httpServer.stop(0);
    }

    @Builder
    private static class HttpExchangeResponseWrapper {
        private HttpExchange exchange;
        private HttpStatus status;
        private String contentType;
        private Map<String, Object> content;
    }
}
