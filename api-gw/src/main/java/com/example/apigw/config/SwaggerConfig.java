package com.example.apigw.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2);
    }

    @Primary
    @Bean
    public SwaggerResourcesProvider swaggerResourcesProvider() {
        return () -> {
            List<SwaggerResource> resources = new ArrayList<>();
            resources.add(newSwaggerResource("/internal-service.swagger.json", "Person-service"));
            resources.add(newSwaggerResource("/other-service.swagger.json", "Other-service"));
            return resources;
        };
    }

    private static SwaggerResource newSwaggerResource(String location, String name) {
        SwaggerResource resource = new SwaggerResource();
        resource.setLocation(location);
        resource.setSwaggerVersion("2.0");
        resource.setName(name);
        return resource;
    }
}
