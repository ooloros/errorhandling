package com.example.apigw.generic;

import lombok.Getter;

import java.util.List;
import java.util.Map;

@Getter
public class BusinessException extends RuntimeException {
    private final transient List<Map<String, Object>> errors;
    private final String mediaType;

    public BusinessException(String mediaType, List<Map<String, Object>> errors) {
        super();
        this.mediaType = mediaType;
        this.errors = errors;
    }
}
