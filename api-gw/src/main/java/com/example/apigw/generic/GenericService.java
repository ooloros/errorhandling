package com.example.apigw.generic;

import com.example.apigw.generic.clientexecutor.RpcExecutor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class GenericService {
    private final RpcExecutor clientExecutor;

    public static record RpcRequest(
            String method,
            String endpoint,
            String accept,
            String contentType,
            Map<String, Object> responseBody
    ) {
    }

    public static record RpcResponse(
            String mediaType,
            Map<String, Object> responseBody
    ) {
    }

    public RpcResponse execute(RpcRequest request) {
        return clientExecutor.execute(request);
    }
}
