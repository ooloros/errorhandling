package com.example.apigw.generic.clientexecutor.http;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
@RequiredArgsConstructor
public class HttpClientConfiguration {
    private final HttpClientConfigurationProperties httpClientConfigurationProperties;

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(httpClientConfigurationProperties.getUrl()));
        return restTemplate;
    }
}
