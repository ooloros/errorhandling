package com.example.apigw.generic;

import com.example.apigw.generic.GenericService.RpcRequest;
import com.example.apigw.generic.GenericService.RpcResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class GenericController {
    private final GenericService genericService;

    @RequestMapping(value = "/rpc/{endpoint}", method = {RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<Map<String, Object>> execute(
            @PathVariable String endpoint,
            @RequestBody Map<String, Object> requestBody,
            HttpServletRequest request
    ) {
        RpcRequest genericRequest = new RpcRequest(request.getMethod(), endpoint, request.getHeader("accept"), request.getContentType(), requestBody);
        RpcResponse genericResponse = genericService.execute(genericRequest);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(Optional.ofNullable(genericResponse.mediaType())
                .map(MediaType::parseMediaType)
                .orElse(MediaType.APPLICATION_JSON));
        return new ResponseEntity<>(genericResponse.responseBody(), headers, HttpStatus.OK);
    }
}
