package com.example.apigw.generic.clientexecutor;

import com.example.apigw.generic.GenericService.RpcRequest;
import com.example.apigw.generic.GenericService.RpcResponse;

public interface RpcExecutor {
    RpcResponse execute(RpcRequest request);
}
