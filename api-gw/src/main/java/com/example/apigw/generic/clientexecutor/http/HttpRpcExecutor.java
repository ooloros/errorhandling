package com.example.apigw.generic.clientexecutor.http;

import com.example.apigw.generic.BusinessException;
import com.example.apigw.generic.GenericService.RpcRequest;
import com.example.apigw.generic.GenericService.RpcResponse;
import com.example.apigw.generic.clientexecutor.RpcExecutor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
public class HttpRpcExecutor implements RpcExecutor {
    private static final ParameterizedTypeReference<Map<String, Object>> TYPE_REF = new ParameterizedTypeReference<>() {
    };
    private final ObjectMapper objectMapper;
    private final RestTemplate restTemplate;

    @Override
    public RpcResponse execute(RpcRequest request) {
        var requestEntity = createRequestEntity(request);
        try {
            var responseEntity = restTemplate.exchange(request.endpoint(), HttpMethod.POST, requestEntity, TYPE_REF);
            return convertToGenericResponse(responseEntity);
        } catch (HttpClientErrorException.BadRequest e) {
            throw convertToBusinessException(e);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    private RuntimeException convertToBusinessException(HttpClientErrorException.BadRequest ex){
        try {
            var respContent = objectMapper.readValue(ex.getResponseBodyAsString(), Map.class);
            var respMediaType = ex.getResponseHeaders().getContentType().toString();
            throw new BusinessException(respMediaType, (List<Map<String, Object>>)respContent.get("errors"));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private RpcResponse convertToGenericResponse(ResponseEntity<Map<String, Object>> response) {
        String mediaType = Objects.requireNonNull(response.getHeaders().getContentType()).toString();
        Map<String, Object> responseBody = response.getBody();
        return new RpcResponse(mediaType, responseBody);
    }

    private HttpEntity<Map<String, Object>> createRequestEntity(RpcRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, request.accept());
        headers.set(HttpHeaders.CONTENT_TYPE, request.contentType());
        return new HttpEntity<>(request.responseBody(), headers);
    }

}

